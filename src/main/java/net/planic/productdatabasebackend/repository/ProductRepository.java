package net.planic.productdatabasebackend.repository;

import net.planic.productdatabasebackend.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
