package net.planic.productdatabasebackend.service;

import net.planic.productdatabasebackend.dto.product.ProductUpdateDto;
import net.planic.productdatabasebackend.model.Product;
import net.planic.productdatabasebackend.repository.ProductRepository;
import net.planic.productdatabasebackend.dto.product.ProductCreateDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product createProduct(ProductCreateDto productCreateDto) {
        Product product = new Product();
        product.setName(productCreateDto.getName());
        product.setPreis(productCreateDto.getPreis());
        String artikelnummer = generateUniqueArtikelnummer();
        product.setArtikelnummer(artikelnummer);
        return productRepository.save(product);
    }

    public Product updateProduct(Long id, ProductUpdateDto productUpdateDto) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            if (productUpdateDto.getName() != null) {
                product.setName(productUpdateDto.getName());
            }
            if (productUpdateDto.getPreis() != null) {
                product.setPreis(productUpdateDto.getPreis());
            }
            return productRepository.save(product);
        } else {
            throw new NoSuchElementException("Produkt nicht gefunden mit der ID: " + id);
        }
    }


    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    public void deleteProducts(List<Long> ids) {
        for (Long id : ids) {
            deleteProduct(id);
        }
    }


    private String generateUniqueArtikelnummer() {
        long timeStamp = System.currentTimeMillis();
        int randomNumber = new Random().nextInt(9000) + 1000;
        return String.format("%d%d", timeStamp, randomNumber);
    }
}
