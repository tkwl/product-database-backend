package net.planic.productdatabasebackend.mapper;

import net.planic.productdatabasebackend.dto.product.ProductResponseDto;
import net.planic.productdatabasebackend.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    public ProductResponseDto toProductResponseDTO(Product product) {
        ProductResponseDto productResponse = new ProductResponseDto();
        productResponse.setId(product.getId());
        productResponse.setName(product.getName());
        productResponse.setArtikelnummer(product.getArtikelnummer());
        productResponse.setPreis(product.getPreis());
        return productResponse;
    }
}
