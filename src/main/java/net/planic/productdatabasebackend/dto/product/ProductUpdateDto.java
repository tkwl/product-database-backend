package net.planic.productdatabasebackend.dto.product;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ProductUpdateDto {

    @NotNull
    @Min(value = 1, message = "Die ID muss positiv sein")
    private Long id;

    private String name;

    private Double preis;
}
