package net.planic.productdatabasebackend.dto.product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductResponseDto {

    private Long id;
    private String name;
    private String artikelnummer;
    private Double preis;

}
