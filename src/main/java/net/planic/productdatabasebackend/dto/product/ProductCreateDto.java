package net.planic.productdatabasebackend.dto.product;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class ProductCreateDto {

    @NotBlank(message = "Name wird benötigt")
    private String name;

    @NotNull(message = "Preis wird benötigt")
    @Positive(message = "Der Preis muss größer als 0 sein")
    private Double preis;

}
