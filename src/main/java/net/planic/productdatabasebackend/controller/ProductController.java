package net.planic.productdatabasebackend.controller;

import net.planic.productdatabasebackend.dto.product.ProductCreateDto;
import net.planic.productdatabasebackend.dto.product.ProductResponseDto;
import net.planic.productdatabasebackend.dto.product.ProductUpdateDto;
import net.planic.productdatabasebackend.model.Product;
import net.planic.productdatabasebackend.service.ProductService;
import net.planic.productdatabasebackend.mapper.ProductMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    public ProductController(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> getAllProducts() {
        List<Product> products = productService.getAllProducts();
        List<ProductResponseDto> productResponses = products.stream()
                .map(productMapper::toProductResponseDTO)
                .collect(Collectors.toList());
        return ResponseEntity.ok(productResponses);
    }

    @PostMapping
    public ResponseEntity<ProductResponseDto> createProduct(@RequestBody ProductCreateDto productCreateDto) {
        Product createdProduct = productService.createProduct(productCreateDto);
        ProductResponseDto productResponse = productMapper.toProductResponseDTO(createdProduct);
        return ResponseEntity.ok(productResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseDto> updateProduct(@PathVariable Long id, @RequestBody ProductUpdateDto productUpdateDto) {
        Product updatedProduct = productService.updateProduct(id, productUpdateDto);
        ProductResponseDto productResponse = productMapper.toProductResponseDTO(updatedProduct);
        return ResponseEntity.ok(productResponse);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteProducts(@RequestBody List<Long> ids) {
        productService.deleteProducts(ids);
        return ResponseEntity.ok().build();
    }
}
